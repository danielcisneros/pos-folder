/**
 * Demonstrate the use of a list to manage a set of objects.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 */

import java.io.IOException;

public class Driver {
    /**
     * Creates and populates a Program of Study. Then saves it using serialization.
     */
    public static void main (String[] args) throws IOException {

    ProgramOfStudy pos = new ProgramOfStudy();

    pos.addCourse(new Course("CS", 101, "Intro to Programming", "A-"));
    pos.addCourse(new Course("ARCH", 305, "Building Analysis", "A"));
    pos.addCourse(new Course("MATH", 201, "Calculus", "C+"));
    pos.addCourse(new Course("BIO", 105, "Biology", "B+"));
    pos.addCourse(new Course("PHY", 101, "Physical Education", "A++"));

    Course arch = pos.find("ARCH", 305); //Testing the find method. PASSED
    System.out.println(arch); //Testing toString method. PASSED

    pos.addCourseAfter(arch, new Course("CSC", 201, "Networking", "A"));//Testing the addCourseAfter method
    System.out.println(pos); //Testing addCourseAfter method. PASSED

    Course net = pos.find("CSC", 201);
    net.setGrade("W"); //Testing setGrade method. PASSED
    System.out.println(net);

    Course bio = pos.find("BIO", 105);
    pos.replace(bio, new Course("CHEM", 101, "Conceptual Chemistry", "D"));
    System.out.println("\n" + pos);
    } //End Main Method
} //End Driver Class