
/**
 * Represents a course that might be taken by a student.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 */

import java.io.Serializable;

public class Course implements Serializable {
    // Declare Variables
    private String prefix;
    private int number;
    private String title;
    private String grade;

    /**
     * Constructs a course with the specified information.
     * 
     * @param prefix the prefix of the course designation
     * @param number the number of the course designation
     * @param title the title of the course
     * @param grade the grade received for the course
     */
    public Course(String prefix, int number, String title, String grade) {
        this.prefix = prefix;
        this.number = number;
        this.title = title;
        if (grade == null) {
            this.grade = "";
        } else {
            this.grade = grade;
        }
    }

    /** 
     * Constructs the course with the specified information, with no grade 
     * established.
     * 
     * @param prefix the prefix of the course designation
     * @param number the number of the course designation
     * @param title the title of the course
     */
    public Course(String prefix, int number, String title) {
        this(prefix, number, title, "");
    }

    /**
     * Returns the prefix of this course.
     * 
     * @return returns the prefix
     */
    public String getPrefix(){
        return prefix;
    }

    /**
     * Returns the number of this course
     * 
     * @return the number of this course
     */
    public int getNumber(){
        return number;
    }

    /**
     * Returns the title of this course.
     * 
     * @return the title of this course
     */
    public String getTitle(){
        return title;
    }

    /**
     * Returns the grade of this course.
     * 
     * @return the grade of this course
     */
    public String getGrade(){
        return grade;
    }

    /**
     * Sets the grade for this course to the one specified.
     * 
     * @param grade the new grade for this course
     */
    public void setGrade(String grade){
        this.grade = grade; 
    }

    /**
     * Returns true if this course has been taken (if a grade is received).
     * 
     * @return true if this course has been taken and false otherwise
     */
    public boolean taken(){
        return !grade.equals("");
    }

    /**
     * Determines if this course is equal to the one specfied, based on the
     * course designation (prefix and number)
     * 
     * @return true is this course is equal to the parameter
     */
    public boolean equals(Object other){
        boolean result = false;
        if (other instanceof Course) {
            Course otherCourse = (Course) other;
            if (prefix.equals(otherCourse.getPrefix()) && number == otherCourse.getNumber()) {
                result = true; 
            }
        }
        return result; 
    }

    /** 
     * Creates and returns a string representation of this course.
     * 
     * @return a string representation of the course
     */
    public String toString() {
        String result = "";
        result += prefix + " "; //Example CSC_
        result += number + " "; //Example CSC_161_
        result += title + " "; //Example CSC_161_Java Foundations_
        if (!grade.equals("")){
            result += "[" + grade + "]"; //Example CSC_161_Java Foundations_[A]_ 
        }
        return result; 
    }
}